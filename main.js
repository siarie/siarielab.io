import fs, {promises as fsp} from "node:fs";
import path from "node:path";

import Processor from "@asciidoctor/core";
import { minify } from 'html-minifier';
import CleanCSS from 'clean-css';

import { Highlight } from './lib/highlight.js';
import { walk, walkSync } from "./lib/util.js";
import config from "./config.js";

const convertOpts = {
  standalone: true,
  safe: "safe",
  template_dirs: "templates",
  template_engine_options: {
    nunjucks: {
      autoescape: false
    }
  },
  attributes: {
    'source-highlighter': 'highlight.js',
  },
};

const asciidoctor = Processor();

const entries = (target) => {
  let e = [];

  walkSync(target, (f, s) => {
    const filename = path.basename(f);
    const ext = f.split(".").pop();
    if (ext == "adoc" && filename !== "index.adoc") {
        const adoc = asciidoctor.loadFile(f);
          const link = f.replace("src/", "/").replace(".adoc", "");
          const desc = adoc.hasAttribute("description")? adoc.getAttribute("description"): "";
          const pubDate = adoc.hasAttribute("published_at")? adoc.getAttribute("published_at"): adoc.getRevisionDate();
          e.push({
            title: adoc.getDocumentTitle(),
            description: desc,
            link,
            pubDate,
          });
    }
  });

  e.sort((a, b) => {
    const da = Date.parse(a.date);
    const db = Date.parse(b.date);

    return da - db;
  }).reverse();

  return e;
};

asciidoctor.SyntaxHighlighter.register('highlight.js', Highlight);
asciidoctor.Extensions.register(function() {
  this.blockMacro(function() {
    var self = this;
    self.named('index');
    self.process(function (parent, target, attrs) {
      const list = entries(target);
      const limit = attrs.limit ? parseInt(attrs.limit): null;
      // console.log(limit);


      let html = `<ul class="custom-marker">`;

      for (let i = 0; i < (list.length);i++) {
          const item = list[i];
          html += `<li data-marker="${item.pubDate} ‣ ">`
          html += `<a href="${item.link}">${item.title}</a></li>`;

          if (limit && limit === (i + 1)) break;
      }
      html += "</ul>";

      return self.createBlock(parent, "pass", html);
    })
  })
});

const main = async () => {
  for await (const f of walk("src")) {
    const ext = f.split(".").pop();
    if (ext == "adoc") {
      const adoc = asciidoctor.load(await fsp.readFile(f), convertOpts);

      const toHTML = (adoc.hasAttribute("index"))
      	  ? f.replace("src/", "public/").replace(".adoc", ".html")
      	  : f.replace("src/", "public/").replace(".adoc", "") + "/index.html";

      const toTXT = f.replace("src/", "public/").replace(".adoc", ".txt")

      if (!adoc.hasAttribute("index")) {
        adoc.setAttribute("rawlink", toTXT.replace("public/", "/"))
      }

      const html = adoc.convert();
      await fsp.mkdir(path.dirname(toHTML), { recursive: true });

      const minHtml = minify(html, {
        collapseWhitespace: true,
        minifyCSS: true,
        removeComments: true,
      });

      if (!adoc.hasAttribute("index")) {
        await fsp.copyFile(f, toTXT)
      }
      await fsp.writeFile(toHTML, minHtml);
    } else {
      const dest = f.replace("src/", "public/");
      await fsp.mkdir(path.dirname(dest), { recursive: true });

      if (ext == 'css') {
        const minCSS = new CleanCSS({}).minify([f]);
        await fsp.writeFile(dest, minCSS.styles);
      } else {
        await fsp.copyFile(f, dest);
      }
    }
  }
}

const clean = async (p) => await fsp.rm(p, {recursive: true});


process.argv.shift();
if (process.argv.length < 2) {
	console.log("error: need 1 argument. found 0.");
	process.exit(1);
}

const task = process.argv[1];
switch(task) {
	case "clean":
		clean("public");
		break;
	case "build":
	default:
		main();
		break;
}
