module.exports.configure = (ctx) => {
  if (ctx.nunjucks && ctx.nunjucks.environment) {
    const env = ctx.nunjucks.environment
    env.addFilter('formatDate', function (str) {
      const d = new Date(str);
      const month = `${d.getMonth()}`.padStart(2, '0');
      const day = `${d.getDay()}`.padStart(2, '0');
      return `${d.getFullYear()}-${month}-${day}`;
    })
  }
}
