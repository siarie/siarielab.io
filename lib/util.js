import fs, { promises as fsp } from 'node:fs'
import path from 'node:path'

export async function* walk(dir) {
  let files = await fsp.readdir(dir)

  for (const file of files) {
    const p = path.join(dir, file)
    const s = await fsp.lstat(p)

    if (s.isDirectory()) yield* await walk(p)
    else yield p
  }
}

export const walkSync = (dir, callback) => {
	const files = fs.readdirSync(dir);
	files.forEach((file) => {
		const filepath = path.join(dir, file);
		const stats = fs.statSync(filepath);
		if (stats.isDirectory()) {
			walkSync(filepath, callback);
		} else if (stats.isFile()) {
			callback(filepath, stats);
		}
	});
};
