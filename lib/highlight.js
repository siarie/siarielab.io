import hljs from 'highlight.js';

export const Highlight = {
  initialize(_name, _backend, { document }) {
    this.super()
  },

  highlight(_node, content, lang) {
    return hljs.highlight(content, {language: lang}).value
  },

  handlesHighlighting() {
    return true
  }
}
